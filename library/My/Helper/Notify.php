<?php
/**
 * Helper para Notificaciones se usa en conjunto con el helper FLASHMESSENGER
 * 
 * USO:
 * al pasar un mensaje en el controller con flashMessenger debes pasar el siguiente esquema de mensaje
 * $flashMessenger=$this->_helper->getHelper('FlashMessenger');
 * $flashMessenger->addMessage('error** No tiene permisos suficientes');
 * el primer mensaje debe tener el tipo de mensaje de notificacion
 * error , alert , success , warning  o  information 
 * seguido por dos asteriscos **
 *  y despues el mensaje a mostrar, con esto es mas que suficiente para mostrar la notificacion
 *  ya que el helper se ejecuta en el masterlayout.phtml
 *  
 * @author jarriaga
 *
 */
class My_Helper_Notify extends Zend_View_Helper_Abstract{
	/**
	 * metodo notify 
	 * obtiene el array de mensajes los parsea y determina el tipo de mensaje
	 * @param unknown_type $mensajes
	 * @return string
	 */
	public function notify($mensajes){
		$codigo="";
		$idnotify=1;
		if($mensajes){
		foreach($mensajes as $mensaje)
		{   
			$mensajex=explode('**',$mensaje);
			switch ($mensajex[0])
			{
				case 'error':
					$codigo.=$this->show_message('error', $mensajex[1],$idnotify);
					break;
				case 'alert':
					$codigo.=$this->show_message('alert', $mensajex[1],$idnotify);
					break;
				case 'success':
					$codigo.=$this->show_message('success', $mensajex[1],$idnotify);
					break;
				case 'warning':
					$codigo.=$this->show_message('warning', $mensajex[1],$idnotify);
					break;
				case 'information':
					$codigo.=$this->show_message('information', $mensajex[1],$idnotify);
					break;
			}
		}
		return $codigo;
		}
	}
	
	/**
	 * Metodo show_message
	 * Esta función muestra guarda el codigo javascript para generar el mensaje con notify
	 * y lo retorna al metodo anterior
	 *
	 * @param unknown_type $type
	 * @param unknown_type $mensaje
	 * @return string
	 */
		private function show_message($type,$mensaje,$id)
			{
				$codigo="var notificacion".$id."= noty({text:'".$mensaje.
				"- [cerrar]',layout:'top',timeout:false, 
					dismissQueue: true,
					force: true,
					animation: {
				    open: {height: 'toggle'},
				    close: {height: 'toggle'},
				    easing: 'swing',
				    speed: 500 // opening & closing animation speed
					  },type: '". $type ."' });";
				return $codigo;		
					
			}
							
							
	
} 