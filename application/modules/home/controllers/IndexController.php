<?php
/**
 * Clase para administrar las paginas home de las organizaciones de futbol
 * @author jarriaga
 *
 */

class home_IndexController extends Zend_Controller_Action{
	
	
	public function init(){
		$this->view->doctype('XHTML1_RDFA');
	}
	/**
	 * Pagina de Home principal de una organizacion
	 * muestra las temporadas y los torneos de cada temporada
	 * asi como los equipos participantes 
	 */
	public function indexAction(){
		$tablaCreador	=	new Application_Model_Creador();
		$tablaTorneos	=	new Application_Model_Torneos();
		
		$organizacion	=	$this->getRequest()->getParam('organizacion');
		
		$informacionCreador	=	$tablaCreador->getOrganizacionByUrl($organizacion);
		$temporadasCreador 	= 	$tablaCreador->getTemporadas($informacionCreador[0]['creadorid']);
		
		$temp			=	array();
		$temporadaid	=	0;
		$indice			=	0;
		
		foreach($temporadasCreador as $temporadas){
			if(	$temporadaid!=$temporadas['temporadaid']){
			   		$temporadaid	=	$temporadas['temporadaid'];
					array_push($temp,array('temporadaid'=>$temporadas['temporadaid'],'nombre'=>$temporadas['nombre']));
			}
			$equipos = $tablaTorneos->getTablaGeneral($temporadas['torneoid']);
			for($i=0;$i<count($equipos);$i++){
				$equipos[$i]['escudo']=
				(file_exists(APPLICATION_PATH.'/../public/media/escudos/'.$equipos[$i]['escudo'])?$equipos[$i]['escudo']:'noimage.jpg');
			}
			$temporadasCreador[$indice]['equipos']=$equipos;
			
			$goleadores = $tablaTorneos->getTorneoGoleadores($temporadas['torneoid']);
			if(!empty($goleadores)){
				$goleadores[0]['fotografia']=
				(file_exists(APPLICATION_PATH.'/../public/media/escudos/'.$goleadores[0]['fotografia'])?$goleadores[0]['fotografia']:'nophoto.jpg');
			$temporadasCreador[$indice]['goleador']	=	array('equipo'=>$goleadores[0]['equipo'],
															   'nombre'=>$goleadores[0]['nombre'],
																'fotografia'=>$goleadores[0]['fotografia'],
																'goles'=>$goleadores[0]['total']	);
			}
			
			$indice++;
		}
		
		$this->view->temporadas			=	$temp;
		$this->view->informacionCreador	=	$informacionCreador[0];
		$this->view->organizacion		=	$organizacion;
		$this->view->temporadasCreador	=	$temporadasCreador;
	}
	
	
	/**
	 * pagina de un torneo especifico 
	 */
	public function torneoAction(){
		$organizacion	=	$this->getRequest()->getParam('organizacion');
		$torneoid=$this->getRequest()->getParam('torneoid');
		$filter = new Zend_Filter_Digits();
		$torneoid = $filter->filter($torneoid);
		
		$this->view->torneoid=$torneoid;
		$this->view->organizacion=$organizacion;
		
		$tablaCreador	=	new Application_Model_Creador();
		$tablaTorneo	=	new Application_Model_Torneos();
		$tablaNoticias 	=	new Application_Model_TorneoNoticias();
		$tablaJornadas 	= 	new Application_Model_Jornadas();		
		
		$informacionTorneo	=	$tablaTorneo->getTorneoInfo($torneoid);	
		$informacionCreador	=	$tablaCreador->getOrganizacionByUrl($organizacion);
		$noticiasTorneo		= 	$tablaNoticias->getNoticias($torneoid);
		$ultimaJornada 		= 	$tablaJornadas->getUltimaJornada($torneoid);
		$goleadoresTorneo	=	$tablaTorneo->getTorneoGoleadores($torneoid);
		$this->view->noticiasTorneo=$noticiasTorneo;
		
		for($i=0;$i<count($ultimaJornada);$i++){
			$ultimaJornada[$i]['escudolocal']=
			(file_exists(APPLICATION_PATH.'/../public/media/escudos/'.$ultimaJornada[$i]['escudolocal'])?$ultimaJornada[$i]['escudolocal']:'noimage.jpg');
			$ultimaJornada[$i]['escudovisita']=
			(file_exists(APPLICATION_PATH.'/../public/media/escudos/'.$ultimaJornada[$i]['escudovisita'])?$ultimaJornada[$i]['escudovisita']:'noimage.jpg');
		}
		$this->view->ultimaJornada=$ultimaJornada;

		if($informacionCreador)
			$this->view->informacionCreador=$informacionCreador[0];
		if($informacionTorneo)
			$this->view->informacionTorneo=$informacionTorneo;
			
	for($i=0;$i<count($goleadoresTorneo);$i++){
			if(!file_exists(APPLICATION_PATH.'/../public/media/jugadores/'.$goleadoresTorneo[$i]['fotografia']))
				$goleadoresTorneo[$i]['fotografia']='nophoto.jpg';
		}
			$this->view->goleadoresTorneo=$goleadoresTorneo;
	}
	
	
	/**
	 * Informacion general de un equipo y organización 
	 */
	
	public function equipoAction(){
		$organizacion=$this->getRequest()->getParam('organizacion');
		$torneoid=$this->getRequest()->getParam('torneoid');
		$filter = new Zend_Filter_Digits();
		$torneoid = $filter->filter($torneoid);
		$equipoid=$this->getRequest()->getParam('equipoid');
		$filter = new Zend_Filter_Digits();
		$equipoid = $filter->filter($equipoid);
		
		$tablaCreador=new Application_Model_Creador();
		$tablaEquipos=new Application_Model_Equipos();
		$tablaTorneo=new Application_Model_Torneos();
		
		
		$informacionEquipo= $tablaEquipos->getEquipoinfo($equipoid);
		$informacionCreador=$tablaCreador->getOrganizacionByUrl($organizacion);
		$estadisticasEquipo=$tablaEquipos->getEstadisticasEquipos($equipoid,$torneoid);
		$informacionTorneo=$tablaTorneo->getTorneoInfo($torneoid);
		$equipoPlantel=$tablaEquipos->getPlantel($equipoid,$torneoid);
		$goleadores=$tablaEquipos->getGoleadoresEquipo($equipoid, $torneoid);
		
		
		if($informacionCreador)
			$this->view->informacionCreador=$informacionCreador[0];
		if($informacionEquipo)
		{
			if(!file_exists(APPLICATION_PATH.'/../public/media/escudos/'.$informacionEquipo['escudo']))
				$informacionEquipo['escudo']='noimage.jpg';

		}
		
		
		for($i=0;$i<count($equipoPlantel);$i++){
			if(!file_exists(APPLICATION_PATH.'/../public/media/jugadores/'.$equipoPlantel[$i]['foto']))
				$equipoPlantel[$i]['foto']='nophoto.jpg';
		}
		
		for($i=0;$i<count($goleadores);$i++){
			if(!file_exists(APPLICATION_PATH.'/../public/media/jugadores/'.$goleadores[$i]['foto']))
				$goleadores[$i]['foto']='nophoto.jpg';
		}
		
		$this->view->informacionEquipo=$informacionEquipo;
		$this->view->informacionTorneo=$informacionTorneo;
		$this->view->estadisticasEquipo=$estadisticasEquipo;
		$this->view->goleadores=$goleadores;
		$this->view->equipoPlantel=$equipoPlantel;
		$this->view->torneoid=$torneoid;
		$this->view->organizacion=$organizacion;
	}
	
	
	
	/**
	 * Funcion que muestra el calendario de un equipo en un torneo específico
	 */
	public function calendarioAction(){
		$organizacion=$this->getRequest()->getParam('organizacion');
		$torneoid=$this->getRequest()->getParam('torneoid');
		$filter = new Zend_Filter_Digits();
		$torneoid = $filter->filter($torneoid);
		$equipoid=$this->getRequest()->getParam('equipoid');
		$filter = new Zend_Filter_Digits();
		$equipoid = $filter->filter($equipoid);
		
		$tablaCreador=new Application_Model_Creador();
		$tablaEquipos=new Application_Model_Equipos();
		$tablaTorneo=new Application_Model_Torneos();
		
		$informacionEquipo= $tablaEquipos->getEquipoinfo($equipoid);
		$informacionCreador=$tablaCreador->getOrganizacionByUrl($organizacion);
		$informacionTorneo=$tablaTorneo->getTorneoInfo($torneoid);
		$calendarioEquipo=$tablaEquipos->getCalendarioEquipo($equipoid,$torneoid);
		
		for($i=0;$i<count($calendarioEquipo);$i++){
			$calendarioEquipo[$i]['escudolocal']=
			(file_exists(APPLICATION_PATH.'/../public/media/escudos/'.$calendarioEquipo[$i]['escudolocal'])?$calendarioEquipo[$i]['escudolocal']:'noimage.jpg');
			$calendarioEquipo[$i]['escudovisita']=
			(file_exists(APPLICATION_PATH.'/../public/media/escudos/'.$calendarioEquipo[$i]['escudovisita'])?$calendarioEquipo[$i]['escudovisita']:'noimage.jpg');
		}
		
		$this->view->torneoid=$torneoid;
		$this->view->organizacion=$organizacion;
		$this->view->informacionEquipo=$informacionEquipo;
		$this->view->informacionTorneo=$informacionTorneo;
		$this->view->informacionCreador=$informacionCreador[0];
		$this->view->calendarioEquipo=$calendarioEquipo;
		
	}
	
	
	/**
	 * Estadisticas de un equipo en un torneo especifico
	 */
	public function estadisticasAction(){
		
		$organizacion=$this->getRequest()->getParam('organizacion');
		$torneoid=$this->getRequest()->getParam('torneoid');
		$filter = new Zend_Filter_Digits();
		$torneoid = $filter->filter($torneoid);
		
		$equipoid=$this->getRequest()->getParam('equipoid');
		
		$filter = new Zend_Filter_Digits();
		$equipoid= $filter->filter($equipoid);
		
		$tablaCreador=new Application_Model_Creador();
		$tablaEquipos=new Application_Model_Equipos();
		$tablaTorneo=new Application_Model_Torneos();
		
		$informacionEquipo= $tablaEquipos->getEquipoinfo($equipoid);
		$informacionCreador=$tablaCreador->getOrganizacionByUrl($organizacion);
		$informacionTorneo=$tablaTorneo->getTorneoInfo($torneoid);
		$juegosjugados=$tablaEquipos->getEstadisticasJuegos($equipoid, $torneoid);
		$juegosganados=$tablaEquipos->getEstadisticasGanados($equipoid,$torneoid);
		$estadisticasEquipo=$tablaEquipos->getEstadisticasEquipos($equipoid,$torneoid);
				
		$this->view->torneoid=$torneoid;
		$this->view->organizacion=$organizacion;
		$this->view->informacionEquipo=$informacionEquipo;
		$this->view->informacionTorneo=$informacionTorneo;
		$this->view->informacionCreador=$informacionCreador[0];
		$this->view->juegosjugados=$juegosjugados;
		$this->view->juegosganados=$juegosganados;
		$this->view->estadisticasEquipo=$estadisticasEquipo;
		
		
	}
	
	
	/**
	 * tablageneral de un torneo 
	 */
	public function tablageneralAction(){
		$organizacion=$this->getRequest()->getParam('organizacion');
		$torneoid=$this->getRequest()->getParam('torneoid');
		$filter = new Zend_Filter_Digits();
		$torneoid = $filter->filter($torneoid);
		$grupo=$this->getRequest()->getParam('grupo',0);
		$filter = new Zend_Filter_Digits();
		$grupo= $filter->filter($grupo);
		
		$tablaCreador=new Application_Model_Creador();
		$tablaTorneo=new Application_Model_Torneos();
		
		$informacionCreador=$tablaCreador->getOrganizacionByUrl($organizacion);
		$informacionTorneo=$tablaTorneo->getTorneoInfo($torneoid);
		$tablaGeneral=$tablaTorneo->getTablaGeneral($torneoid,$grupo);
		$this->view->torneoid=$torneoid;
		$this->view->organizacion=$organizacion;
		$this->view->informacionTorneo=$informacionTorneo;
		$this->view->informacionCreador=$informacionCreador[0];
		
		for($i=0;$i<count($tablaGeneral);$i++){
			$tablaGeneral[$i]['escudo']=
			(file_exists(APPLICATION_PATH.'/../public/media/escudos/'.$tablaGeneral[$i]['escudo'])?$tablaGeneral[$i]['escudo']:'noimage.jpg');
		}
		
		$this->view->tablaGeneral=$tablaGeneral;
	}
	
	/**
	 * Muestra los resultados de la jornada en curso
	 */
	public function resultadosjornadasAction(){
		$organizacion=$this->getRequest()->getParam('organizacion');
		$torneoid=$this->getRequest()->getParam('torneoid');
		$grupo=$this->getRequest()->getParam('jornada',0);
		
		$tablaCreador = new Application_Model_Creador();
		$tablaTorneo = new Application_Model_Torneos();
		$tablaJornadas = new Application_Model_Jornadas();
		
		$informacionCreador=$tablaCreador->getOrganizacionByUrl($organizacion);
		$informacionTorneo=$tablaTorneo->getTorneoInfo($torneoid);
		$resultadosJornadas=$tablaJornadas->getResultadosJornadas($torneoid);
		
		
		
		for($i=0;$i<count($resultadosJornadas);$i++){
			$resultadosJornadas[$i]['escudolocal']=
			(file_exists(APPLICATION_PATH.'/../public/media/escudos/'.$resultadosJornadas[$i]['escudolocal'])?$resultadosJornadas[$i]['escudolocal']:'noimage.jpg');
			$resultadosJornadas[$i]['escudovisita']=
			(file_exists(APPLICATION_PATH.'/../public/media/escudos/'.$resultadosJornadas[$i]['escudovisita'])?$resultadosJornadas[$i]['escudovisita']:'noimage.jpg');
		}

		$this->view->torneoid=$torneoid;
		$this->view->organizacion=$organizacion;
		$this->view->informacionTorneo=$informacionTorneo;
		$this->view->informacionCreador=$informacionCreador[0];
		$this->view->resultadosJornadas=$resultadosJornadas;
	}
	
	
	/**
	 *  Equipos del torneo 
	 * 
	 */
	
	public function equiposAction(){
		$organizacion=$this->getRequest()->getParam('organizacion');
		$torneoid=$this->getRequest()->getParam('torneoid');
		$filter = new Zend_Filter_Digits();
		$torneoid = $filter->filter($torneoid);
		
		$tablaCreador = new Application_Model_Creador();
		$tablaTorneo = new Application_Model_Torneos();
		$tablaEquipo = new Application_Model_Equipos();

		$informacionCreador=$tablaCreador->getOrganizacionByUrl($organizacion);
		$informacionTorneo=$tablaTorneo->getTorneoInfo($torneoid);
		$equiposTorneo = $tablaEquipo->getEquiposTorneo($torneoid);
		
		for($i=0;$i<count($equiposTorneo);$i++){
			$equiposTorneo[$i]['escudo']=
			(file_exists(APPLICATION_PATH.'/../public/media/escudos/'.$equiposTorneo[$i]['escudo'])?$equiposTorneo[$i]['escudo']:'noimage.jpg');
		}
		
		$this->view->torneoid=$torneoid;
		$this->view->organizacion=$organizacion;
		$this->view->informacionTorneo=$informacionTorneo;
		$this->view->informacionCreador=$informacionCreador[0];
		$this->view->equiposTorneo = $equiposTorneo;
	}
	
	/**
	 *  Detalle del partido
	 */
	public function partidoAction(){
		$organizacion=$this->getRequest()->getParam('organizacion');
		$torneoid=$this->getRequest()->getParam('torneoid');
		$filter = new Zend_Filter_Digits();
		$torneoid = $filter->filter($torneoid);
		$partidoid=$this->getRequest()->getParam('partidoid');
		$filter = new Zend_Filter_Digits();
		$partidoid= $filter->filter($partidoid);
		
		$tablaCreador = new Application_Model_Creador();
		$tablaTorneo = new Application_Model_Torneos();
		$tablaEquipo = new Application_Model_Equipos();
		
		
		$informacionCreador=$tablaCreador->getOrganizacionByUrl($organizacion);
		$informacionTorneo=$tablaTorneo->getTorneoInfo($torneoid);
		$informacionPartido=$tablaEquipo->getPartidoInfo($partidoid);
		$informacionEventos=$tablaEquipo->getEventosPartido($partidoid);
		
			
		$this->view->torneoid=$torneoid;
		$this->view->organizacion=$organizacion;
		$this->view->informacionTorneo=$informacionTorneo;
		$this->view->informacionCreador=$informacionCreador[0];
		$this->view->informacionPartido=$informacionPartido;
		$this->view->informacionEventos=$informacionEventos;
		
	}
	
}