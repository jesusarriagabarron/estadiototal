<?php
class admin_PartidosController extends Zend_Controller_Action {
	private $sesion;
		
	
	public function init(){
		$this->view->doctype('XHTML1_RDFA');
		$this->sesion= new Zend_Session_Namespace('sesion');
		$this->view->flashmessenger=$this->_helper->flashMessenger->getMessages();
	}
	
	
	/**
	 * Crea un partido y devuelve los datos del partido
	 */
	public function crearpartidoAction(){
		$request = $this->getRequest();
		$usuario = $this->sesion->usuario;
		$post 	= $request->getPost();
		$filter = new Zend_Filter_Digits();
		$torneoid = $filter->filter($post['torneoid']);
		$jornadaid = $filter->filter($post['jornadaid']);
		
		$this->_helper->json($post);
		
	}
	
	
	/**
	 * Eliminar una jornada específica
	 */
	public function eliminarjornadaAction(){
		$request = $this->getRequest();
		$usuario = $this->sesion->usuario;
		$post = $request->getPost();
		$filter = new Zend_Filter_Digits();
		$torneoid = $filter->filter($post['deltorneoid']);
		$jornadaid = $filter->filter($post['jornadaid']);
		$tablaCreador = new Application_Model_Creador();
		if($request->isPost() && $tablaCreador->esMiTorneo($torneoid, $usuario['creadorid'])){
			$tablaJornadas = new Application_Model_Jornadas();
			$tablaPartidos = new Application_Model_Partidos();
			$tablaArbitrosPartidos = new Application_Model_Arbitrospartido();
			
			$jornadainfo=$tablaJornadas->getJornadainfo($jornadaid);
			if($jornadainfo['torneoid']==$torneoid){
				$partidos = $tablaPartidos->getPartidos($jornadaid);
				foreach($partidos as $partido){
					//borramos arbitros asignados
					$tablaArbitrosPartidos->delete('partidoid='.$partido['partidoid']);		
				}
				//borramos partidos de la jornada
				$tablaPartidos->delete('jornadaid='.$jornadaid);
				//borramos la jornada
				$tablaJornadas->delete('jornadaid='.$jornadaid.' and torneoid='.$torneoid);
				//borramos cache de jornadas
				$tablaJornadas->getJornadas($torneoid,1);
				$flashMessenger=$this->_helper->getHelper('FlashMessenger');
				$flashMessenger->addMessage('success** La Jornada se eliminó exitosamente ');
				$this->_redirect("/admin/partidos/nuevo/".$torneoid);
			}
			$this->_helper->json($jornadaid);
		}
	}
	
	/**
	 * crear una nueva jornada
	 */
	public function nuevajornadaAction(){
		$request = $this->getRequest();
		$tablaCreador = new Application_Model_Creador();
		$usuario = $this->sesion->usuario;
		$post = $request->getPost();
		$filter = new Zend_Filter_Digits();
		$torneoid = $filter->filter($post['torneoid']);
		
		if($request->isPost() && $tablaCreador->esMiTorneo($torneoid, $usuario['creadorid']) && $post['njornada']){
			$filter = new Zend_Filter_Digits();
			$njornada = $filter->filter($post['njornada']);
			$fechai = explode('/',$post['fechainicio']);
			$fechaf = explode('/',$post['fechafin']);

			$datearray = array (
						'year' 	=>	$fechai[2],
						'month'	=>	$fechai[1],
						'day'	=>	$fechai[0]
					);
			$fechainicio = new Zend_Date($datearray);
			
			$datearray = array (
					'year' 	=>	$fechaf[2],
					'month'	=>	$fechaf[1],
					'day'	=>	$fechaf[0]
			);
			$fechafinal = new Zend_Date($datearray);
			
			$tablaJornadas = new Application_Model_Jornadas();
			$data = array(
								'torneoid'	  => 	$torneoid,
								'fechainicio' =>	$fechainicio->toString("YYYY-MM-dd"),
								'fechafin'	  => 	$fechafinal->toString("YYYY-MM-dd"),
								'numerojornada' => 	$njornada
						);
			
			$tablaJornadas->insert($data);
			$tablaJornadas->getJornadas($torneoid,1);
			
			
			$flashMessenger=$this->_helper->getHelper('FlashMessenger');
    		$flashMessenger->addMessage('success** La Jornada '.$njornada. ' se creó exitosamente');
			$this->_redirect("/admin/partidos/nuevo/".$torneoid);
			
		}else{
			$this->_redirect("/admin/partidos/nuevo/".$torneoid);
			$njornada=0;
		}
		
		$this->_helper->json($njornada);
		
	}
	
	/**
	 * Mostrar equipos de un torneo especifico
	 */
	public function mostrarequiposAction(){
		$request = $this->getRequest();
		$usuario = $this->sesion->usuario;
		$post = $request->getPost();
		$filter = new Zend_Filter_Digits();
		$torneoid = $filter->filter($post['torneoid']);
		$tablaCreador = new Application_Model_Creador();
		if($request->isPost() && $tablaCreador->esMiTorneo($torneoid, $usuario['creadorid'])){
			$tablaEquipos = new Application_Model_Equipos();
			$equipos = $tablaEquipos->getEquiposTorneo($torneoid);
			$this->_helper->json($equipos);
		}	
	}
	
	
	/**
	 * Obtiene partidos
	 */
	public function obtienepartidosAction(){
		$request = $this->getRequest();
		$usuario = $this->sesion->usuario;
		$post = $request->getPost();
		$filter = new Zend_Filter_Digits();
		$torneoid = $filter->filter($post['torneoid']);
		$jornadaid = $filter->filter($post['jornadaid']);
		$tablaCreador = new Application_Model_Creador();
		if($request->isPost() && $tablaCreador->esMiTorneo($torneoid, $usuario['creadorid'])){
			$tablaJornadas = new Application_Model_Jornadas();
			$tablaPartidos = new Application_Model_Partidos();
				
			$jornadainfo=$tablaJornadas->getJornadainfo($jornadaid);
			if($jornadainfo['torneoid']==$torneoid){
				$partidos=$tablaPartidos->getPartidosInfo($jornadaid);
				$this->_helper->json($partidos);				
			}
		}
	}
	/*
	 *	Funcion para crear un nuevo partido 
	 *	para un torneo especifico 
	 */
	public function nuevoAction(){
		$torneoid = $this->getRequest()->getParam("torneoid",0);
		
			$tablaCreador = new Application_Model_Creador();
			$usuario = $this->sesion->usuario;
			// comprobar si el torneo es mio
			if(!$tablaCreador->esMiTorneo($torneoid, $usuario['creadorid'])){
				$flashMessenger=$this->_helper->getHelper('FlashMessenger');
    			$flashMessenger->addMessage('error** El Torneo que buscas no existe');
				$this->_redirect("/admin");
			}
			
			
			$tablaEquipos = new Application_Model_Equipos();
			$this->view->equipos = $tablaEquipos->getEquiposTorneo($torneoid);


			$tablaJornadas =  new Application_Model_Jornadas();
			$tablaTorneo =  new Application_Model_Torneos();

			$this->view->jornadas = $tablaJornadas->getJornadas($torneoid,0);
			$this->view->torneoinfo = $tablaTorneo->getTorneoInfo($torneoid);
			$this->view->torneoid = $torneoid;
			
			
	}
}