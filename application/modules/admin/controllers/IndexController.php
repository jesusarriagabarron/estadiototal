<?php
/**
 * Clase para el sistema de administración de un creador 
 * @author jarriaga
 *
 */

class admin_IndexController extends Zend_Controller_Action{
	
	private $sesion;
	
	
	public function init(){
		$this->view->doctype('XHTML1_RDFA');
		$this->sesion= new Zend_Session_Namespace('sesion');
		$this->view->flashmessenger=$this->_helper->flashMessenger->getMessages();
	}
	/**
	 * Dashboard principal
	 */
	public function indexAction(){
		$usuario = $this->sesion->usuario;
		$tablaCreador =  new Application_Model_Creador();
		$this->view->torneos = $tablaCreador->getDashboardTorneos($usuario['creadorid']);
	}
}