<?php
class admin_TorneosController extends	Zend_Controller_Action{

	private $sesion;
	

	
	public function init(){
		$this->view->doctype('XHTML1_RDFA');
		$this->sesion= new Zend_Session_Namespace('sesion');
		$this->view->flashmessenger=$this->_helper->flashMessenger->getMessages();
	
	}
	

	/**
	 * Torneo index
	 */
	public function indexAction(){
		$torneoid = $this->getRequest()->getParam("torneoid",0);
		$filter = new Zend_Filter_Digits();
		$torneoid = $filter->filter($torneoid);
		$usuario = $this->sesion->usuario;
		
		$tablaCreador = new Application_Model_Creador()	;
			$usuario = $this->sesion->usuario;
			if(!$tablaCreador->esMiTorneo($torneoid, $usuario['creadorid'])){
				$flashMessenger=$this->_helper->getHelper('FlashMessenger');
    			$flashMessenger->addMessage('error**Upps el Torneo que buscas no existe');
				$this->_redirect("/admin");
			}
		
		$tablaCreador =  new Application_Model_Creador();
		$tablaTorneo =  new Application_Model_Torneos();
		$tablaTorneoNoticias = new Application_Model_TorneoNoticias();
		$this->view->noticias = $tablaTorneoNoticias->getNoticias($torneoid);
		$this->view->torneoinfo = $tablaTorneo->getTorneoInfo($torneoid);
		$this->view->torneoid = $torneoid;
	}
	
	/*
	 * Elimina las noticias del torneo
	 */
	public function eliminarnoticiaAction(){
		$torneoid = $this->getRequest()->getParam("torneoid",0);
		$noticiaid  = $this->getRequest()->getParam("noticiaid","");
		if($torneoid){
			$tablaCreador = new Application_Model_Creador()	;
			$usuario = $this->sesion->usuario;
			if($tablaCreador->esMiTorneo($torneoid, $usuario['creadorid'])){
				$tablaTorneoNoticias = new Application_Model_TorneoNoticias();
				$tablaTorneoNoticias->delete("idTorneo_noticia=".$noticiaid. " and idTorneo=".$torneoid);
			}
		}
		$r=1;
		$this->_helper->json->sendJson($r);
	}
	/*
	 * Procesa el formulario para publicar una nueva noticia
	 */
	public function cargarnoticiaAction(){
		$torneoid = $this->getRequest()->getParam("torneoid",0);
		$noticia  = $this->getRequest()->getParam("noticia","");
		$filter = new Zend_Filter_StripTags();
		$noticia = $filter->filter($noticia);
		if($torneoid){
			$tablaCreador = new Application_Model_Creador();
			$usuario = $this->sesion->usuario;
			if($tablaCreador->esMiTorneo($torneoid,$usuario['creadorid'])){
				$tablaTorneoNoticias = new Application_Model_TorneoNoticias();
				$date = new Zend_Date();
				$data = array(
							'idTorneo' 	=> $torneoid,
							'cTitulo'	=>	'',
							'cTexto'	=>	$noticia,
							'cFecha'	=>	$date->toString('YYYY-MM-dd HH:mm:ss'),
							'iPublicada'=>	1
						);
				$noticiaid=$tablaTorneoNoticias->insert($data);
				$resultado = array('response' => array('noticia'=>$noticia,
														'fecha'=>$date->toString('YYYY-MM-dd HH:mm:ss'),
														'noticiaid'=>$noticiaid));
			}else{
				$resultado = array('response'=>'torneo erroneo');
			}
		}
		else
			$resultado = array('response'=>'no existe el torneo');
	
		$this->_helper->json->sendJson($resultado);
	}
}