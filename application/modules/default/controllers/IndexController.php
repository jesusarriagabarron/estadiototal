<?php

include "My/Facebooksdk/facebook.php";

class IndexController extends Zend_Controller_Action
{

	public $config;
	private $sesion;
	
	public function preDispatch(){
	$this->view->flashmessenger=$this->_helper->flashMessenger->getMessages();
	}
	
    public function init()
    {
    	$this->sesion= new Zend_Session_Namespace('sesion');
    	$this->config = array(
    				'appId' 	=> 	Zend_Registry::get('appid'),
    				'secret'	=>	Zend_Registry::get('appsecret'),
    				'cookie'	=>	true
    			);
    	
	   	$this->view->doctype('XHTML1_RDFA');
        /* Initialize action controller here */
    }

    public function indexAction()
    {
    	$auth=Zend_Auth::getInstance();
		$modeloTorneo  = new Application_Model_Torneos();
		$modeloCreador = new Application_Model_Creador();
		
		$ultimostorneos=$modeloTorneo->getUltimostorneosInfo(4,0);
		$this->view->ultimostorneos=$ultimostorneos;
		
		if($this->getRequest()->isPost()){
				$usuario = $this->getRequest()->getPost('user');
				$pass = $this->getRequest()->getPost('pass');
				$authAdapter=new Zend_Auth_Adapter_DbTable($modeloCreador->getAdapter(),'creador');
				$authAdapter->setIdentityColumn('usuario')->setCredentialColumn('contrasena')
				->setIdentity($usuario)->setCredential($pass);
				$authAdapter->setCredentialTreatment(" md5(?) AND estado=1");
				$resultado = $auth->authenticate($authAdapter);
		
				if($resultado->isValid())
				{
					$sesion= new Zend_Session_Namespace('sesion');
					$row= (array) $authAdapter->getResultRowObject();
					$dataUsuario=$modeloCreador->getCreador($row['creadorid']);
					$sesion->usuario=$dataUsuario[0];
					$this->view->usuario=$sesion->usuario;
				}else{
					$flashMessenger=$this->_helper->getHelper('FlashMessenger');
					$flashMessenger->addMessage('error** El usuario y/o contraseña es incorrecto');
				}
				$this->_redirect("/");
		}
		
    }
    
    public function torneosAction(){
    	$modeloTorneo=new Application_Model_Torneos();
    	
    	$ultimostorneos=$modeloTorneo->getUltimostorneosInfo();
    	$this->view->ultimostorneos=$ultimostorneos;
    	$page = $this->getRequest()->getParam('page', 1);
    	$this->view->page=$page;
    	
    }
    
    
	public function logoutAction()
    {
    	$auth=Zend_Auth::getInstance();
    	$auth->clearIdentity();
        $this->_redirect("/");
    }
    
    public function queesAction(){
    	
    }
    
    public function contactoAction(){
    	
    }
    
    
    /**
     * Recepcion del correo de confirmación, este action recibe el link
     * donde se confirma la cuenta y se activa	
     */
    public function confirmacionAction(){
    	$modeloCreador = new Application_Model_Creador();
    	$request = $this->getRequest();
    	$email = $request->getParam("email");
    	$unique = $request->getParam("id");
    	if(!empty($email) && !empty($unique)){
    		$data=array(
    				"estado"=>1
    				);
    		$response=$modeloCreador->update($data, "correo='".$email."' and codigo='".$unique."'");
    		
    		if($response){
    			$flashMessenger=$this->_helper->getHelper('FlashMessenger');
    			$flashMessenger->addMessage('success** Tu cuenta ha sido activada exitosamente '.$email);
    		}else{
    			$flashMessenger=$this->_helper->getHelper('FlashMessenger');
    			$flashMessenger->addMessage('error** Esta cuenta ya ha sido activada');
    		}
    		$this->_redirect("/");
    	}
    }
    
    /**
     * Registro 
     */
    public function registroorganizadorAction(){
    	//$form = new Application_Form_registro();
    	//$this->view->form=$form;
    	$modeloCreador = new Application_Model_Creador();
    	$request = $this->getRequest();
    	
    	if($request->getParam("confirmacion")==1){
    		$this->view->confirmacion=1;
    	}
    	else{
    	
    	$signed_request= $this->_getParam('signed_request');
    	
    	if(!empty($signed_request)){
    		$response = $this->ParseRequest($_REQUEST['signed_request'], Zend_Registry::get('appsecret'));
    		$this->view->response = $response;
    		$unique = uniqid();
    		
    		if(!$modeloCreador->getYaexiste($response['registration']['email'])){
    		
		    		$data = array(
		    				'usuario'		=>	$response['registration']['email'],
		    				'contrasena'	=>	md5($response['registration']['password']),
		    				'nombre'		=>	strtoupper($response['registration']['name']),
		    				'correo'		=>	$response['registration']['email'],
		    				'pais'			=>	$response['user']['country'],
		    				'codigo'		=>	$unique,
		    				'estado'		=>	0,
		    				'contrasenalimpia'	=>	$response['registration']['password']	
		    				);
		    		
					$this->view->idCreador=$modeloCreador->insert($data);
		    		//enviar correo de confirmacion de correo y validación	
		    		if(!empty($this->view->idCreador)){
						    $script = '{
						    "key": "WUrmTORsFEIptHO5iwQaAQ",
						    "template_name": "Registro-EstadioTotal",
						    "template_content": [
						        {
						            "name": "example name",
						            "content": "example content"
						        }
						    ],
						    "message": {
						         "subject": "Activa tu cuenta",
						        "from_email": "registro@estadiototal.com",
						        "from_name": "EstadioTotal.com",
						        "to": [
						            {
						                "email": "'.$response['registration']['email'].'",
						                "name": "'.strtoupper($response['registration']['name']).'"
						            }
						        ],
						        "headers": {
						            "Reply-To": "admin@estadiototal.com"
						        },
						        "important": false,
						        "track_opens": null,
						        "track_clicks": null,
						        "auto_text": null,
						        "auto_html": null,
						        "inline_css": null,
						        "url_strip_qs": null,
						        "preserve_recipients": null,
						        "bcc_address": "message.bcc_address@example.com",
						        "tracking_domain": null,
						        "signing_domain": null,
						        "merge": true,
						        "global_merge_vars": [
						            {
						                "name": "merge1",
						                "content": "codigo - 234t1dc"
						            }
						        ],
						        "merge_vars": [
						            {
						                "rcpt": "'.$response['registration']['email'].'",
						                "vars": [
						                    {
						                        "name" : "TWITTER",
						                        "content" : "https://twitter.com/estadio_total"
						                    },
						                    {
						                        "name": "FACEBOOK",
						                    	"content": "http://www.facebook.com/estadiototal"
						                    },
						                    {
						                        "name" : "COMPANY",
						               			"content" : "EstadioTotal.com"
						                    },
						                    {
						                        "name" : "NUESTROEMAIL",
						                		"content" : "admin@estadiototal.com"
						                    },
						                    {
						                         "name" : "LINKCONFIRMACION",
						                		"content" : "http://www.estadiototal.com/confirmacion?email='.$response['registration']['email'].'&id='.$unique.'"
						                    }
						                ]
						            }
						        ],
						        "tags": [
						            "Mensaje de confirmacion"
						        ]
						    },
						    "async": false
							}';
    			 
			    			$uri="https://mandrillapp.com/api/1.0/messages/send-template.json";
			    			$ch = curl_init();
			    			curl_setopt($ch, CURLOPT_URL, $uri);
			    			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
			    			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
			    			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    			curl_setopt($ch, CURLOPT_POST, true);
			    			curl_setopt($ch, CURLOPT_POSTFIELDS, $script);
			    			$result = curl_exec($ch);
			    			
			    			$this->_redirect("/registro-organizador?confirmacion=1");
			    			
			    			}	//fin if idCreador
    				} // fin existe
    				else{
    					$flashMessenger=$this->_helper->getHelper('FlashMessenger');
    					$flashMessenger->addMessage('error** El email o usuario ya esta registrado ');
    					$this->_redirect("/registro-organizador");
    				}
   		 	} // fin if empty signed_request
    
    	}
    }

    private function ParseRequest($signed_request, $secret){
    	list($encoded_sig, $payload) = explode('.', $signed_request, 2);
    	// decode the data
    	$sig = $this->base64_url_decode($encoded_sig);
    	$data = json_decode($this->base64_url_decode($payload), true);
    	if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
    		error_log('Unknown algorithm. Expected HMAC-SHA256');
    		return null;
    	}
    	$expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
    	if ($sig !== $expected_sig) {
    		error_log('Bad Signed JSON signature!');
    		return null;
    	}
    	
    	return $data;
    }
    
    private function base64_url_decode($input) {
    	return base64_decode(strtr($input, '-_', '+/'));
    }

}

