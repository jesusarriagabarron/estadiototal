<?php
/**
 * Clase que crea el formulario para dar de alta un nuevo emisor en el sistema
 * @author jarriaga
 *
 */
class Application_Form_registro extends Zend_Form{
	
	public $mensajes;
	
	public function init(){
			
		$this->setMethod('post');
		
		
		$nombre = new Zend_Form_Element_Text('nombre');
		$nombre->setLabel('Nombre completo: ')
		->setRequired(true)
		->setAttrib('size','80')->setAttrib('class', 'input-xlarge')
		->addFilter('StringToUpper');
		

		$email = new Zend_Form_Element_Text('email');
		$email->setLabel('Email: ')
		->setRequired(true)->setAttrib('class', 'input-large')
		->addFilter('StringToUpper');

		
		$usuario = new Zend_Form_Element_Text('usuario');
		$usuario->setLabel('Usuario: ')
		->setRequired(true)->setAttrib('class', 'input-medium');
		
				
		$passwd = new Zend_Form_Element_Password('password');
		$passwd->setLabel('Contraseña: ')
			->setRequired(true)->setAttrib('class', 'input-medium');
		
		$passwdconf = new Zend_Form_Element_Password('password2');
		$passwdconf->setLabel('Confirma tu contraseña: ')
			->setRequired(true)->setAttrib('class', 'input-medium')
			->addValidator('Identical',false,array('token'=>'password'));
			
			
		
		$pais = new Zend_Form_Element_Select('estado');
		$pais->setLabel('Estado');
		$listapaises = new Application_Model_Creador();
		$paises=$listapaises->getPaises();
		foreach($paises as $p){
			$pais->addMultiOptions( array($p['ID']=>$p['NOMBRE']));

		}
		
		
		$this->addElements(
				array(
						$nombre,
						$email,
						$pais,
						$usuario,
						$passwd,
						$passwdconf
				)
		);
		
		
		
		
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Guardar Emisor')->setAttrib('class', 'btn btn-large');
		$decorate = array(
				array('ViewHelper'),
				array('Description'),
				array('Errors',array('style'=>'margin:-11px 0 20px 0px!important;')),
				array('HtmlTag', array('tag' => 'div', 'placement'=>'IMPLICIT_PREPEND','class'=>'text-center'))
				);
		$submit->setDecorators($decorate);
		
		$this->addElement($submit);
		
		
	}
}
