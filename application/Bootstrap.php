<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

	protected function _initView()
	{
		$autoLoader = Zend_Loader_Autoloader::getInstance();
		$autoLoader->registerNamespace('My_');
	
		Zend_Locale::setDefault('es_MX');
		/**
		 * Define la ruta de los helpers views
		 * @var unknown_type
		*/
		$view = new Zend_View();
		$view->setEncoding('UTF-8');
		$view->addHelperPath('../library/My/Helper','My_Helper');
		$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
		$viewRenderer->setView($view);
	
	/*	$helper= new My_Controller_Helper_Acl();
		$helper->setRoles();
		$helper->setResources();
		$helper->setPrivilages();
		$helper->setAcl();
	
		
		$front->registerPlugin(new My_Controller_Plugin_Acl());
	*/
	
		$this->_rutas();
		
		/**
		 * Manejo de caché de datos
		 */
		
			$frontendOptions = array(
			   'lifetime' => 7200,
			   'automatic_serialization' => true
			);
			 
			$backendOptions = array(
			    'cache_dir' => APPLICATION_PATH.'/../cache'
			);
			 
			$CacheGeneral = Zend_Cache::factory('Core',
			                             'File',
			                             $frontendOptions,
			                             $backendOptions);
			 
			Zend_Registry::set('cache',$CacheGeneral);
			Zend_Registry::set('appid','131312660336014');
			Zend_Registry::set('appsecret','225dc430eff2b8e307622d94c3c2b219');
			
		
		return $view;
	}
	
	
	protected function _rutas(){
			
		$hostname="my.phpcloud.com/version2/";
		$frontController= Zend_Controller_Front::getInstance();
		$router=$frontController->getRouter();
		
		$subdominios_home=new Zend_Controller_Router_Route_Hostname(
				":organizacion.".$hostname,
				array(
						'module'=>'default',
						'controller'=>'home',
						'action'=>'index'
						)
				);
		$plainPathRoute=new Zend_Controller_Router_Route_Static('');
		
		$router->addRoute('Homes',$subdominios_home->chain($plainPathRoute));

		$ruta_torneos = new Zend_Controller_Router_Route('/torneos/pagina/:page',
						array(	'module'=>'default',
								'controller'=>'index',
								'action'=>'torneos'
							)
						);
		
		$router->addRoute('torneos',$ruta_torneos);
		
		$router->addRoute('torneohome',new Zend_Controller_Router_Route('/:organizacion/torneo/:torneoid',
									array(
										'module'=>'home',
										'controller'=>'index',
										'action'=>'torneo'	
										)));
		
		$router->addRoute('torneoequipo',
				new Zend_Controller_Router_Route('/:organizacion/torneo/:torneoid/equipo/:equipoid',
				array(
						'module'=>'home',
						'controller'=>'index',
						'action'=>'equipo'
				)));
	
		$router->addRoute('calendarioequipo',
				new Zend_Controller_Router_Route('/:organizacion/torneo/:torneoid/equipo/:equipoid/calendario',
						array(
								'module'=>'home',
								'controller'=>'index',
								'action'=>'calendario'
						)));
		
		$router->addRoute('estadisticasequipo',
				new Zend_Controller_Router_Route('/:organizacion/torneo/:torneoid/equipo/:equipoid/estadisticas',
						array(
								'module'=>'home',
								'controller'=>'index',
								'action'=>'estadisticas'
						)));
		$router->addRoute('tablageneraltorneo',
				new Zend_Controller_Router_Route('/:organizacion/torneo/:torneoid/tablageneral',
						array(
								'module'=>'home',
								'controller'=>'index',
								'action'=>'tablageneral'
						)));
		$router->addRoute('tablageneralgrupotorneo',
				new Zend_Controller_Router_Route('/:organizacion/torneo/:torneoid/tablageneral/grupo/:grupo',
						array(
								'module'=>'home',
								'controller'=>'index',
								'action'=>'tablageneral'
						)));
		
		$router->addRoute('resultadosjornadastorneo',
				new Zend_Controller_Router_Route('/:organizacion/torneo/:torneoid/jornadas',
						array(
								'module'=>'home',
								'controller'=>'index',
								'action'=>'resultadosjornadas'
						)));
		
		$router->addRoute('equipostorneo',
				new Zend_Controller_Router_Route('/:organizacion/torneo/:torneoid/equipos',
						array(
								'module'=>'home',
								'controller'=>'index',
								'action'=>'equipos'
						)));
		
		$router->addRoute('partidotorneo',
				new Zend_Controller_Router_Route('/:organizacion/torneo/:torneoid/partido/:partidoid',
						array(
								'module'=>'home',
								'controller'=>'index',
								'action'=>'partido'
						)));
		
		$router->addRoute('organizacion',
				new Zend_Controller_Router_Route('/:organizacion',
						array(
								'module'=>'home',
								'controller'=>'index',
								'action'=>'index'
						)));
						
		$router->addRoute('registro-organizador',
				new Zend_Controller_Router_Route('/registro-organizador',
						array(
								'module'=>'default',
								'controller'=>'index',
								'action'=>'registroorganizador'
						)));
	
		$router->addRoute('quees',
				new Zend_Controller_Router_Route('/que_es',
						array(
								'module'=>'default',
								'controller'=>'index',
								'action'=>'quees'
						)));
		
		$router->addRoute('contacto',
				new Zend_Controller_Router_Route('/contacto',
						array(
								'module'=>'default',
								'controller'=>'index',
								'action'=>'contacto'
						)));
		
		$router->addRoute('confirmacion',
				new Zend_Controller_Router_Route('/confirmacion',
						array(
								'module'=>'default',
								'controller'=>'index',
								'action'=>'confirmacion'
						)));
		
		$router->addRoute('administraccion',
				new Zend_Controller_Router_Route('/admin',
						array(
								'module'=>'admin',
								'controller'=>'index',
								'action'=>'index'
						)));
		
		$router->addRoute('admin_torneo',
				new Zend_Controller_Router_Route('/admin/torneos/:torneoid',
						array(
								'module'=>'admin',
								'controller'=>'torneos',
								'action'=>'index'
						)));
		
		$router->addRoute('admin_nuevopartido',
				new Zend_Controller_Router_Route('/admin/partidos/nuevo/:torneoid',
						array(
								'module'=>'admin',
								'controller'=>'partidos',
								'action'=>'nuevo'
						)));						
						
	}
	
	protected function _initSession()
	{
		Zend_Session::start();
	}
	
}

