<?php


class Application_Model_Jornadas extends Zend_Db_Table_Abstract{
	
	protected $_primary = 'jornadaid';
	protected $_name ='jornada';
	
	/**
	 * Crear el cache de un sistema!!!
	 * @param unknown_type $SQL
	 * @param unknown_type $IdCache
	 * @param unknown_type $tiempo
	 * @param unknown_type $modificado
	 * @throws Exception
	 * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
	 */
	private function cacheSQL($SQL,$IdCache,$tiempo=1800,$modificado=0){
		//creamos la versión del cache
		$cache= Zend_Registry::get('cache');
		$cache_clave  = md5($IdCache);
		if($modificado)
			$cache->remove($cache_clave);
		$datos = $cache->load($cache_clave);
		if(false===$datos){
			try{
				$datos=$this->getAdapter()->query($SQL)->fetchAll();
				$cache->setLifetime($tiempo);
				$cache->save($datos,$cache_clave);
			}catch(Exception $e){
				 throw new Exception('<pre>No fue posible obtener información de la base de datos'  .$e . '</pre>', 666);
			}
		}
		return $datos;	
	}
	

	/**
	 * Obtiene las jornadas de un torneo especifico ordenados
	 * de modo ascendente
	 * @param unknown_type $torneoid
	 * @param unknown_type $modificado
	 */
	public function getJornadas($torneoid,$modificado=0){
		$sql = "
				SELECT		jornadaid,torneoid,fechainicio,fechafin,numerojornada,publicada,fase,nota
				FROM		jornada
				WHERE		torneoid={$torneoid}
				ORDER BY	numerojornada ASC
		";	
		
		$resultado =  $this->cacheSQL($sql,'getJornadas'.$torneoid,1800,$modificado);
		return $resultado;
		
	}
	
	/**
	 * Informacion de una jornada especifica
	 * @param unknown_type $jornadaid
	 */
	public function getJornadainfo($jornadaid){
		$jornada=$this->fetchAll('jornadaid='.$jornadaid)->toArray();
		if($jornada)
			$jornada=$jornada[0];
		else
			$jornada=null;
		return $jornada;		
	}
	
	/**
	 * Obtiene los resultados de todas las jornadas
	 * @param unknown_type $torneoid
	 * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
	 */
	public function getResultadosJornadas($torneoid){
		$sql="	SELECT		p.estadioid as estadioid, 
							(
								SELECT	nombre 
							 	FROM	estadio 
							 	WHERE	estadioid=p.estadioid
							) as nombreestadio,
							j.jornadaid as jornadaid,
							j.fechainicio as fechainicio,
							p.partidoid as partidoid,
							p.equipolocalid as localid,
							p.equipovisitaid as visitaid,
							(
								SELECT	nombre 
								FROM	equipo 
								WHERE	equipoid=p.equipolocalid 
							) as local,
 							(select nombre from equipo where equipoid=p.equipovisitaid ) as visita,
  							(select escudo from equipo where equipoid=p.equipolocalid ) as escudolocal,
 							(select escudo from equipo where equipoid=p.equipovisitaid ) as escudovisita,
 							p.fechahora as fechahora, 
							p.goleslocal as goleslocal,
							p.golesvisita as golesvisita,
							p.tiporesultadoid as resultadoid, 
 							j.fechainicio as fechajornada,
							j.numerojornada as njornada 
				FROM		jornada j
  				LEFT JOIN	partido p  
				ON			p.jornadaid=j.jornadaid 
				WHERE		j.torneoid={$torneoid} 
				ORDER BY	j.numerojornada ASC,p.fechahora ASC";
		$resultado = $this->cacheSQL($sql,'ResultadosJornadas'.$torneoid,300);
		return $resultado;
	}
	
	/**
	 * Obtiene la ultima jornada disponible
	 * @param unknown_type $torneoid
	 * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
	 */
	public function getUltimaJornada($torneoid){
		$sql=	"SELECT 	j.jornadaid,p.tiporesultadoid,j.numerojornada,j.publicada,j.nota,j.fechainicio,j.fechafin,
							p.partidoid,p.equipolocalid,
							(SELECT e.nombre FROM equipo e WHERE e.equipoid=p.equipolocalid) as equipolocal,
							(SELECT e.escudo FROM equipo e WHERE e.equipoid=p.equipolocalid) as escudolocal,
							p.equipovisitaid,
							(SELECT e.nombre FROM equipo e WHERE e.equipoid=p.equipovisitaid) as equipovisita,
							(SELECT e.escudo FROM equipo e WHERE e.equipoid=p.equipovisitaid) as escudovisita,
							p.goleslocal,p.golesvisita,
							p.fechahora,p.estadioid
				FROM		jornada j
				INNER JOIN 	partido p 
				ON 			j.jornadaid=p.jornadaid
				WHERE		j.torneoid={$torneoid} and fechafin<=NOW() and fechafin=
							(	SELECT 	max(fechafin)
								FROM		jornada 
								WHERE		torneoid={$torneoid} and fechafin<=NOW()
							)
				ORDER BY p.fechahora ASC";
		
		$resultado=$this->getAdapter()->query($sql)->fetchAll();
		return $resultado;
	}
	
}
