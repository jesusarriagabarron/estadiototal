<?php
/**
 * Modelo para manejo de Torneos
 * @author jarriaga
 *
 */

class Application_Model_Torneos extends Zend_Db_Table_Abstract{
	
	protected $_name = 'torneo';
	protected $_primary ='torneoid';

	
	/**
	 * Obtiene los ultimos torneos ordenados descendentemente  con o son limite
	 * @param unknown_type $count
	 * @param unknown_type $offset
	 */
	public function getUltimostorneos($count=0,$offset=0){	
			if($count)
				$resultado=$this->fetchAll($this->select()->order('torneoid DESC')->limit($count,$offset));
			else
				$resultado=$this->fetchAll($this->select()->order('torneoid DESC'));
			return $resultado->toArray();
	}
	
	/**
	 * Obtiene los torneos, creadores y datos de la pagina
	 * informacion para la pagina principal
	 * @param unknown_type $count
	 * @param unknown_type $offset
	 */
	public function getUltimostorneosInfo($count=0,$offset=0){
		$select ="	SELECT t.torneoid, t.nombre AS nombretorneo, t.tipotorneo, s.nombre AS nombretemporada, 
							p.organizacion AS nombreorganizacion, p.logo as logo,p.nombre as organizacionurl,
							(SELECT count(equipoid)  
							 FROM torneo_equipo where torneoid=t.torneoid) as totalequipos
					FROM torneo t	
					INNER JOIN temporada s 
					ON t.temporadaid = s.temporadaid
					INNER JOIN pagina p 
					ON s.creadorid = p.creadorid 
					ORDER BY t.torneoid DESC ";
		if($count)
				$select.=" LIMIT {$offset},{$count}";
		
		
		$resultado=$this->getAdapter()->query($select)->fetchAll();
		return $resultado;
	}
	
	
	/**
	 * Obtiene todos los datos de la tabla torneo de un torneo especifico
	 * @param unknown_type $torneoid
	 */
	public function getTorneoInfo($torneoid){
		$torneo=$this->fetchAll('torneoid='.$torneoid)->toArray();
		if($torneo)
			$torneo=$torneo[0];
		else
			$torneo=null;
		return $torneo;		
	}
	
	
	/**
	 * Obtiene la lista de goleadores de un torneo específico
	 * @param unknown_type $torneoid
	 */
	public function getTorneoGoleadores($torneoid){
		$sql="	SELECT			count(ti.nombreid ) as  total,
								i.equipoid as equipoid,
								i.jugadorid as jugadorid,
								e.nombre as equipo,
								concat(j.nombre,' ',j.apellidop,' ',j.apellidom) as nombre,
								j.fotografia as fotografia,
								i.torneoid as torneoid
				FROM 			incidencia i 
				INNER JOIN		tipoincidencia ti 
				ON				i.tipoincidenciaid=ti.tipoincidenciaid 
				INNER JOIN		jugador j 
				ON				i.jugadorid=j.jugadorid
				INNER JOIN		equipo e 
				ON				e.equipoid = i.equipoid
				WHERE 			ti.nombreid='GOL' 
				AND				i.torneoid={$torneoid}
				GROUP BY		jugadorid,nombre,equipoid,equipo,torneoid 
				ORDER BY		total 
				DESC LIMIT 10";
		
		$goleadores=$this->getAdapter()->query($sql)->fetchAll();
		return $goleadores;
	}
	
	
	/**
	 * Tabla general de un torneo
	 * @param unknown_type $torneoid
	 * @param unknown_type $grupo
	 */
	public function getTablaGeneral($torneoid,$grupo=0){
		$where="";
		if($grupo && is_numeric($grupo))
			$where=" AND te.grupo={$grupo} ";
		$sql="	SELECT				te.torneoid as torneoid, 
									te.equipoid as equipoid,
									e.nombre as nombre,
									e.escudo as escudo,
									ifnull(count(t.equipoid),0) as JJ,
									ifnull(sum(t.pg),0) as JG,
									ifnull(sum(t.pp),0) as JP,
									ifnull(sum(t.pe),0) as JE,
									ifnull(sum(t.goles_afavor),0) as GF,
									ifnull(sum(t.goles_encontra),0) as GC,
									ifnull( (sum(t.goles_afavor)-  sum(t.goles_encontra)),0) as DIF,
									ifnull((sum(t.puntos) + ( sum(t.puntosextra))),0) as PTS
    			FROM				torneo_equipo te 
    			LEFT JOIN			resultados t  
    			ON					te.torneoid=t.torneoid 
    			AND					t.equipoid=te.equipoid 
    			INNER JOIN			equipo e 
    			ON					te.equipoid=e.equipoid 
    			WHERE				te.torneoid={$torneoid} {$where}
 				GROUP BY			te.torneoid,
 									te.equipoid,
 									e.nombre 
 				ORDER BY			PTS desc, DIF desc
				";
		
		$datos = $this->cacheSQL($sql, 'getTablaGeneral'.$torneoid);
		return $datos;
		
	}
	
	
	/**
	 * Administra el caché de una consulta especifica
	 * @param unknown_type $SQL
	 * @param unknown_type $IdCache
	 * @param unknown_type $tiempo
	 */
	private function cacheSQL($SQL,$IdCache,$tiempo=1800){
		//creamos la versión del cache
	
		$cache= Zend_Registry::get('cache');
		$cache_clave  = md5($IdCache);
		
		$datos = $cache->load($cache_clave);
		if(false==$datos){
			try{
				//$datos=$this->query($SQL);
				$datos=$this->getAdapter()->query($SQL)->fetchAll();
				$cache->setLifetime($tiempo);
				$cache->save($datos,$cache_clave);
			}catch(Exception $e){
				 throw new Exception('<pre>No fue posible obtener información de la base de datos'  .$e . '</pre>', 666);
			}
		}
		return $datos;	
	}
	
}