<?php

/**
 * Clase para la consulta de equipos 
 * @author jarriaga
 *
 */
class Application_Model_Equipos extends Zend_Db_Table_Abstract{
	
	protected $_primary = 'equipoid';
	protected $_name ='equipo';
	
	/**
	 * Información de un equipo de la tabla equipo
	 * @param unknown_type $equipoid
	 * @return Ambigous <NULL, unknown, multitype:>
	 */
	public function getEquipoinfo($equipoid){
		$equipo=$this->fetchAll('equipoid='.$equipoid)->toArray();
		if($equipo)
			$equipo=$equipo[0];
		else
			$equipo=null;
		return $equipo;		
	}
	
	
	/**
	 * Obtiene los equipos del torneo
	 * 
	 * @param unknown_type $torneoid
	 * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
	 */
	public function getEquiposTorneo($torneoid){
		$SQL="	SELECT 			e.equipoid,
								e.nombre, 
								e.fechafundacion, 
								e.presidente,
								e.directortecnico, 
								e.escudo, 
								e.telefono, 
								e.codigopostal, 
								e.ciudad, 
								e.direccion, 
								e.correo
				FROM 			equipo e
				INNER JOIN 		torneo_equipo te 
				ON 				e.equipoid = te.equipoid
				WHERE te.torneoid ={$torneoid}";
		return $this->getAdapter()->query($SQL)->fetchAll();
	}
	
	/**
	 * Obtener la plantilla de jugadores de un equipo
	 * 
	 * @param unknown_type $equipoid
	 * @param unknown_type $torneoid
	 * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|NULL
	 */
	public function getPlantel($equipoid,$torneoid){
		if(is_numeric($equipoid) && is_numeric($torneoid)){
		$sql="	SELECT 			jet.torneoid as torneoid,
								jet.equipoid as equipoid,
								jet.jugadorid as jugadorid,
								concat(j.nombre,' ',j.apellidop,' ',j.apellidom) as nombre,
								j.fotografia as foto,
								j.fechanacimiento as fechanacimiento,
								j.peso	as peso,
								j.estatura as estatura,
								j.posicion as posicion,
								j.ciudadnacimiento as nacimiento  
				FROM			jugador_equipo_temporada jet 
				INNER JOIN		jugador j 
				ON 				jet.jugadorid = j.jugadorid 
				WHERE			jet.torneoid=".$torneoid."
				AND				jet.equipoid=".$equipoid." 
				ORDER BY		j.nombre ";
		return $this->getAdapter()->query($sql)->fetchAll();
		}
		else	
			return null;
		
	}
	
	
	/**
	 * Obtiene las estadisticas de un equipo 
	 * juegos jugados,ganados,perdidos,goles a favor, en conrta, diferencia, puntos.
	 * 
	 * @param unknown_type $equipoid
	 * @param unknown_type $torneoid
	 * @return unknown|NULL
	 */
	public function getEstadisticasEquipos($equipoid,$torneoid){
		
		if(is_numeric($equipoid) && is_numeric($torneoid)){
			$sql="
				SELECT		te.torneoid as torneoid,
					 		te.equipoid as equipoid ,
							e.nombre as nombre,
							e.escudo as escudo,
							ifnull(count(t.equipoid),0) as JJ,
							ifnull(sum(t.pg),0) as JG,
							ifnull(sum(t.pp),0) as JP,
							ifnull(sum(t.pe),0) as JE,
							ifnull(sum(t.goles_afavor),0) as GF,
							ifnull(sum(t.goles_encontra),0) as GC,
							ifnull( (sum(t.goles_afavor)-  sum(t.goles_encontra)),0) as DIF,
							ifnull((sum(t.puntos) + ( sum(t.puntosextra))),0) as PTS
				FROM	 	torneo_equipo te 
				LEFT JOIN	resultados t  
				ON			te.torneoid=t.torneoid 
				AND		    t.equipoid=te.equipoid 
				INNER JOIN	equipo e 
				ON			te.equipoid=e.equipoid 
				WHERE		te.torneoid={$torneoid} 
				AND 		te.equipoid={$equipoid} 
				GROUP BY	te.torneoid,te.equipoid,e.nombre 
				ORDER BY	PTS desc, DIF DESC";
			$estadisticas=$this->getAdapter()->query($sql)->fetchAll();
			$estadisticas=$estadisticas[0];
			return $estadisticas;
		}
		else
			return null;
	}
	
	/**
	 * Obtiene los goleadores de un equipo especifico
	 * @param unknown_type $equipoid
	 * @param unknown_type $torneoid
	 */
	public function getGoleadoresEquipo($equipoid,$torneoid){
		if(is_numeric($equipoid) && is_numeric($torneoid)){
		$sql="	SELECT 			count(i.jugadorid) as goles,
								concat(j.nombre,' ',j.apellidop,' ',j.apellidom) as nombre,
								j.jugadorid as jugadorid,
								j.fotografia as foto 
				FROM 			incidencia i 
				INNER JOIN		jugador j 
				ON				i.jugadorid=j.jugadorid 
				WHERE			i.torneoid=".$torneoid." 
				AND				i.equipoid=".$equipoid."
 				AND				i.tipoincidenciaid=
						(	SELECT		tipoincidenciaid 
							FROM		tipoincidencia 
							WHERE		nombreid='GOL'
						) 
				GROUP BY		j.nombre,
								j.jugadorid,
								j.fotografia 
				ORDER BY		goles DESC LIMIT 3";
				$goleadores=$this->getAdapter()->query($sql)->fetchAll();
				return $goleadores;
		
		}
		else
			return null;
	}
	
	
	/**
	 * 
	 * Obtiene el calendario de un equipo especifico en un torneo
	 * @param unknown_type $equipoid
	 * @param unknown_type $torneoid
	 */
	
	public function getCalendarioEquipo($equipoid,$torneoid){
		if(is_numeric($equipoid) && is_numeric($torneoid)){
			$sql="		SELECT
									j.jornadaid as jornadaid,
									p.partidoid as partidoid,
									p.equipolocalid as localid,
									p.equipovisitaid as visitaid,
									p.goleslocal as goleslocal,
									p.golesvisita as golesvisita,
									p.fechahora as fechahora,
									p.tiporesultadoid as tiporesultadoid,
									(SELECT nombre FROM equipo WHERE equipoid=p.equipolocalid ) as local,
									(SELECT nombre FROM equipo WHERE equipoid=p.equipovisitaid ) as visita,
									(SELECT escudo FROM equipo WHERE equipoid=p.equipolocalid ) as escudolocal,
									(SELECT escudo FROM equipo WHERE equipoid=p.equipovisitaid ) as escudovisita
						FROM		jornada j 
						INNER JOIN	partido p  
						ON			j.jornadaid=p.jornadaid 
						WHERE		j.torneoid={$torneoid}
						AND			(p.equipolocalid={$equipoid} or p.equipovisitaid={$equipoid}) 
						ORDER BY	fechahora 
						ASC
					";
			$calendario=$this->getAdapter()->query($sql)->fetchAll();
			return $calendario;
		}
		else 
				return null;
	}
	
	
	/**
	 * Funcion que regresa los juegos jugados cuantos ganados, cuantos perdidos y cuantos empates
	 * @param unknown_type $equipoid
	 * @param unknown_type $torneoid
	 * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|NULL
	 */
	public function getEstadisticasJuegos($equipoid,$torneoid){
		if(is_numeric($equipoid) && is_numeric($torneoid)){
			$sql= "	SELECT			te.equipoid,
									ifnull(sum(r.pg),0) as ganados,
									ifnull(sum(r.pp),0) as perdidos,
									ifnull(sum(r.pe),0) as empates
					FROM			torneo_equipo te 
					LEFT JOIN		resultados r 
					ON				te.equipoid=r.equipoid 
					AND				te.torneoid=r.torneoid
					WHERE 			te.torneoid={$torneoid} and te.equipoid={$equipoid}
					GROUP BY		te.equipoid";
			$estadisticas= $this->getAdapter()->query($sql)->fetchAll();
			return $estadisticas;		
		}
		else
			return null;
	}
	
	/**
	 * Obtiene toda la información de un partido
	 * @param unknown_type $partidoid
	 * @return string
	 */
	
	public function getPartidoInfo($partidoid){
		$SQL = "	SELECT			partidoid,
									jornadaid,
									equipolocalid,
									(
										SELECT		nombre 
										FROM		equipo 
										WHERE		equipoid=partido.equipolocalid 
									) as equipolocal,
									equipovisitaid,
									(
										SELECT		nombre 
										FROM		equipo 
										WHERE		equipoid=partido.equipovisitaid 
									) as equipovisita,
									(
										SELECT		escudo 
										FROM		equipo 
										WHERE		equipoid=partido.equipolocalid 
									) as equipolocalescudo,
									(	
										SELECT 		escudo 
										FROM		equipo 
										WHERE		equipoid=partido.equipovisitaid 
									) as equipovisitaescudo,
									goleslocal,
									golesvisita,
									tegoleslocal,
									tegolesvisita, 
									penallocal,
									penalvisita,
									tiporesultadoid,
									tipotiempoid,
									fechahora,
									observaciones,
									fase,
									puntoslocal,
									puntosvisita,
									puntosextralocal,
									puntosextravisita,
									estadioid,  
									(
										SELECT		nombre 
										FROM		estadio 
										WHERE		estadioid=partido.estadioid
									) as estadio,  
									(
										SELECT		fotografia 
										FROM		estadio 
										WHERE		estadioid=partido.estadioid
									) as fotoestadio  
						FROM		partido 
						WHERE		partidoid={$partidoid}";
		
		$data=$this->getAdapter()->query($SQL)->fetchAll();
		$data=$data[0];
		$info['partido']=$data;
		
		if($data['estadioid']){
		
			$SQL="	SELECT		nombre,direccion,ciudad,capacidad,fotografia,latitud,longitud,superficie
					FROM		estadio
					WHERE		estadioid={$data['estadioid']}
					";
		$data=$this->getAdapter()->query($SQL)->fetchAll();
		$data=$data[0];
		$terreno='';
		switch($data['superficie'])
		{
			case 0:
				$terreno='Cesped Natural';break;
			case 1:
				$terreno='Cesped Artificial';break;
			case 2:
				$terreno='Tierra';break;
			case 3:
				$terreno='Concreto';break;
			case 4:
				$terreno='Duela';break;
		}
		$data['terreno']=$terreno;
		$info['estadio']=$data;
		}

		$SQL="	SELECT		a.nombre as nombre,a.fotografia as fotografia,ap.tipo as tipo,
							ap.partidoid as partidoid 
				FROM 		arbitro_partido ap 
				INNER JOIN	arbitro a 
				ON			ap.arbitroid=a.arbitroid 
				WHERE		ap.partidoid={$partidoid}";
		$data=$this->getAdapter()->query($SQL)->fetchAll();
		if($data)
		$data=$data[0];
		$info['arbitro']=$data;
		
		return $info;
		
	}
	
	/**
	 * Obtiene los partidos ganados de local y de visita
	 * @param unknown_type $equipoid
	 * @param unknown_type $torneoid
	 */
	public function getEstadisticasGanados($equipoid,$torneoid){
		if(is_numeric($equipoid) && is_numeric($torneoid)){
			$sql="	SELECT			te.equipoid,
									count(p.tiporesultadoid) as total 
					FROM			torneo_equipo te   
					LEFT JOIN		partido p 
					ON				te.equipoid=p.equipovisitaid 
					OR				te.equipoid=p.equipolocalid 
					INNER JOIN		jornada j 
					ON				p.jornadaid=j.jornadaid 
					AND				te.torneoid=j.torneoid
					WHERE			te.torneoid={$torneoid} 
					AND				( p.equipolocalid={$equipoid}) 
					AND				te.equipoid={$equipoid}
					AND 			p.tiporesultadoid='L'
				";
			
			$ganados['ganadoslocal']=$this->getAdapter()->query($sql)->fetchAll();
			$ganados['ganadoslocal']=$ganados['ganadoslocal'][0]['total'];
			
			$sql="	SELECT 			te.equipoid,
									count(p.tiporesultadoid) as total 
					FROM			torneo_equipo te   
					LEFT JOIN		partido p 
					ON				te.equipoid=p.equipovisitaid 
					OR				te.equipoid=p.equipolocalid 
					INNER JOIN 		jornada j 
					ON				p.jornadaid=j.jornadaid 
					AND				te.torneoid=j.torneoid
					WHERE			te.torneoid={$torneoid}
					AND				(p.equipovisitaid={$equipoid} ) 
					AND				te.equipoid={$equipoid}
					AND				p.tiporesultadoid='V'
					";
			$ganados['ganadosvisita']=$this->getAdapter()->query($sql)->fetchAll();
			$ganados['ganadosvisita']=$ganados['ganadosvisita'][0]['total'];			
			
			
			$sql="	SELECT			te.equipoid,
									count(p.tiporesultadoid) as total 
					FROM			torneo_equipo te   
					LEFT JOIN		partido p 
					ON				te.equipoid=p.equipovisitaid 
					OR				te.equipoid=p.equipolocalid 
					INNER JOIN		jornada j 
					ON				p.jornadaid=j.jornadaid 
					AND				te.torneoid=j.torneoid
					WHERE			te.torneoid={$torneoid} 
					AND				( p.equipolocalid={$equipoid}) 
					AND				te.equipoid={$equipoid}
					AND 			p.tiporesultadoid='V'
				";
			$ganados['perdidoslocal']=$this->getAdapter()->query($sql)->fetchAll();
			$ganados['perdidoslocal']=$ganados['perdidoslocal'][0]['total'];

			$sql="	SELECT			te.equipoid,
									count(p.tiporesultadoid) as total 
					FROM			torneo_equipo te   
					LEFT JOIN		partido p 
					ON				te.equipoid=p.equipovisitaid 
					OR				te.equipoid=p.equipolocalid 
					INNER JOIN		jornada j 
					ON				p.jornadaid=j.jornadaid 
					AND				te.torneoid=j.torneoid
					WHERE			te.torneoid={$torneoid} 
					AND				( p.equipovisitaid={$equipoid}) 
					AND				te.equipoid={$equipoid}
					AND 			p.tiporesultadoid='L'
				";
			$ganados['perdidosvisita']=$this->getAdapter()->query($sql)->fetchAll();
			$ganados['perdidosvisita']=$ganados['perdidosvisita'][0]['total'];
			
			return $ganados;
			
		}
		else return null;
	}
	
	
	/**
	 * Devuelve los eventos de un partido especifico
	 * @param unknown_type $partidoid
	 */
	public function getEventosPartido($partidoid){
		$SQL="	SELECT 			i.incidenciaid as incidenciaid, 
								i.partidoid as partidoid, 
								ti.descripcion as descripcion,
								ti.icono as icono, 
								i.minuto as minuto, 
								e.nombre as equipo, 
								j.nombre as jugador,
								j.apellidop as jugadorap,
								j.apellidom as jugadoram 
				FROM			incidencia i
				INNER JOIN		tipoincidencia ti 
				ON				i.tipoincidenciaid=ti.tipoincidenciaid  
				INNER JOIN		equipo e 
				ON			 	i.equipoid=e.equipoid 
				LEFT JOIN		jugador j 
				ON				i.jugadorid=j.jugadorid 
				WHERE			i.partidoid={$partidoid}
				ORDER BY		i.minuto asc";
		return $this->getAdapter()->query($SQL)->fetchAll();
	}
	
}
