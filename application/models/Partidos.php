<?php
class Application_Model_Partidos extends Zend_Db_Table_Abstract{
		
	protected $_primary = 'partidoid';
	protected $_name ='partido';

	/**
	 * Cache sql
	 */
	private function cacheSQL($SQL,$IdCache,$tiempo=1800,$modificado=0){
		//creamos la versión del cache
		$cache= Zend_Registry::get('cache');
		$cache_clave  = md5($IdCache);
		if($modificado)
			$cache->remove($cache_clave);
		$datos = $cache->load($cache_clave);
		if(false===$datos){
			try{
				$datos=$this->getAdapter()->query($SQL)->fetchAll();
				$cache->setLifetime($tiempo);
				$cache->save($datos,$cache_clave);
			}catch(Exception $e){
				throw new Exception('<pre>No fue posible obtener información de la base de datos'  .$e . '</pre>', 666);
			}
		}
		return $datos;
	}
	
	/**
	 * Todo la informacion de jornada
	 * @param unknown_type $jornadaid
	 * @param unknown_type $remove
	 */
	public function getPartidosInfo($jornadaid,$remove=0){
		$sql = "	SELECT		partidoid,jornadaid,
								(	SELECT	nombre 
									FROM 	equipo	
									WHERE	equipoid=partido.equipolocalid) as nombrelocal,
								(	SELECT	nombre 
									FROM 	equipo	
									WHERE	equipoid=partido.equipovisitaid) as nombrevisita,
								equipolocalid,equipovisitaid,
		goleslocal,golesvisita,tiporesultadoid,tipotiempoid,fechahora,
		estadioid,observaciones,fase,puntoslocal,puntosvisita,
		puntosextralocal,puntosextravisita
		FROM		partido
		WHERE		jornadaid={$jornadaid}	";
		$resultado = $this->cacheSQL($sql, 'getPartidos'.$jornadaid,$remove);
		return $resultado;
	}
	/**
	 * Obtiene los partidos
	 * @param unknown_type $torneoid
	 * @param unknown_type $modificado
	 */
	public function getPartidos($jornadaid,$modificado=0,$remove=0){
		$sql = "	SELECT		partidoid,jornadaid,equipolocalid,equipovisitaid,
								goleslocal,golesvisita,tiporesultadoid,tipotiempoid,fechahora,
								estadioid,observaciones,fase,puntoslocal,puntosvisita,
								puntosextralocal,puntosextravisita
					FROM		partido
					WHERE		jornadaid={$jornadaid}	";
		$resultado = $this->cacheSQL($sql, 'getPartidos'.$jornadaid,$remove);
		return $resultado;
	}
	
}