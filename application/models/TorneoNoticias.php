<?php

class Application_Model_TorneoNoticias extends Zend_Db_Table_Abstract{
	
	protected $_name = "torneo_noticias";
	protected $_primary ="idTorneo_noticia";
	
	/*
	 *  Obtiene las noticias de un torneo especifico 
	 */
	public function getNoticias($idTorneo){
		if(is_numeric($idTorneo)){
			return $this->fetchAll('idTorneo='.$idTorneo.' and iPublicada=1','cFecha DESC')->toArray();
		}
		else
			return false;
	}
	
	
}