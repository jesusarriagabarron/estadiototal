<?php
/**
 * Modelo para administrar la tabla creador y todo lo relacionado con el creador
 * de una organizacion de futbol
 * @author jarriaga
 *
 */

class Application_Model_Creador extends Zend_Db_Table_Abstract{
	
	protected $_name='creador';
	protected $_primary='creadorid';

	/**
	 * Verifica si un torneo pertenece al creador
	 * @param unknown_type $torneoid
	 * @param unknown_type $creadorid
	 */
	public function esMiTorneo($torneoid,$creadorid){
		$sql = " 	SELECT	 	count(tor.torneoid) as total 
					FROM 		torneo tor
					INNER JOIN	temporada tem
					ON			tor.temporadaid=tem.temporadaid
					INNER JOIN	creador cre
					ON 			cre.creadorid=tem.creadorid
					WHERE		cre.creadorid={$creadorid}
					AND			tor.torneoid={$torneoid}";
		$resultado = $this->getAdapter()->query($sql)->fetchAll();
		$resultado = $resultado[0];
		if($resultado['total']>0)
			return true;
		else
			return false;
			
	}
	
	/*
	 * verifica si un correo ya existe en el registro de usuarios
	 */
	public function getYaexiste($email){
		$sql = "	SELECT 	count(creadorid) as contador 
					FROM 	creador
					WHERE	usuario='{$email}' 
					OR		correo='{$email}'
				";
		$resultado = $this->getAdapter()->query($sql)->fetchAll();
		$resultado = $resultado[0];
		if($resultado['contador']>0)
			return true;
		else
			return false;
	}	
	
	public function getCreador($creadorid){
		$sql = "	SELECT	creadorid,usuario,nombre,correo,pais,ultimoingreso,estado
					FROM	creador
					WHERE	creadorid={$creadorid}";
		$resultado = $this->getAdapter()->query($sql)->fetchAll();
		return $resultado;
	}
	
	public function getPaises(){
		$sql = "	SELECT		ID,ISO3,NOMBRE
					FROM		pais
				";
		$resultado=$this->getAdapter()->query($sql)->fetchAll();
		return $resultado;
	}
	/*
	 * Obtiene los datos de un creador especifico por su nombre de URL
	 */		
	public function getOrganizacionByUrl($nombreurl){
		$select =	"	SELECT		p.paginaid,p.creadorid,p.nombre as nombreurl,p.organizacion,p.facebook,
									p.twitter,p.logo,c.nombre as nombrecreador,c.correo as emailcreador,
									c.pais
						FROM		pagina p 
						INNER JOIN	creador c
						ON 			p.creadorid=c.creadorid
						WHERE		p.nombre='{$nombreurl}'";
		$resultado=$this->getAdapter()->query($select)->fetchAll();
		return $resultado;
	}
	
	/**
	 * Obtiene las temporadas de un torneo específico
	 */	
	public function getTemporadas($creadorid){
		$sql ="	SELECT		te.temporadaid,te.nombre,tor.nombre as nombretorneo,tor.torneoid
				FROM		temporada te
				INNER JOIN  torneo tor
				ON			tor.temporadaid = te.temporadaid
				WHERE		te.creadorid={$creadorid} 
				ORDER BY	te.temporadaid DESC, tor.torneoid DESC ";
		$resultado = $this->getAdapter()->query($sql)->fetchAll();
		return $resultado;
	}
	
	/**
	 * Obtiene los torneos,temporadas y equipos participantes de un creador específico
	 */
	public function getDashboardTorneos($creadorid){
		$sql ="	SELECT te.temporadaid, te.nombre, tor.nombre AS nombretorneo, tor.torneoid, 
				(
					SELECT IFNULL( COUNT( equipoid ) , 0 ) 
					FROM torneo_equipo
					WHERE torneoid = tor.torneoid
				) AS participantes
				FROM temporada te
				INNER JOIN torneo tor ON tor.temporadaid = te.temporadaid
				WHERE te.creadorid ={$creadorid}
				ORDER BY te.temporadaid DESC , tor.torneoid DESC ";
		$resultado = $this->getAdapter()->query($sql)->fetchAll();
		return $resultado;
	}
	
}